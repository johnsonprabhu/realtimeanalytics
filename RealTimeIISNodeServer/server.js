﻿var app = require('http').createServer(handler), io = require('socket.io').listen(app), path = require('path'), fs = require('fs'), und = require('underscore'), hubs = require('hubs/hubs');

io.configure(function () {
    io.set('transports', ['websocket']);
    if (process.env.IISNODE_VERSION) {
        // If this node.js application is hosted in IIS, assume it is hosted 
        // in IIS virtual directory named 'dante' and set up the socket.io's resource
        // value for socket.io to recognize requests that target it. 
        // Note a corresponding change in the client index-socketio.html, as well
        // as necessary URL rewrite rule in web.config. 

        io.set('resource', '/socket.io');
    }
});

function handler(req, res) {
    fs.readFile(path.resolve(__dirname, 'index.html'),
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }

            res.writeHead(200);
            res.end(data);
        }
    );
}

hubs.init(io, und, 'iisnode_');


app.listen(process.env.PORT || 9999);
