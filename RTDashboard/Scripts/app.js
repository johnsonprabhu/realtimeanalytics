﻿define(['signalR', 'socketio', 'moment'], function (signalR, socketio, moment) {
    console.log("SignalR version: ", $.signalR.version);
    console.log("SocketIO version: ", io.version);
    
    var dashboardViewModel = (function() {
        var that = this;
        this.trackingId = '';
        this.serverInfo = ko.observable('None Selected');
        this.realtimeServer = ko.observable('');
        this.dataItems = ko.observableArray();
        this.users = ko.observableArray();
        this.summary = {};
        this.summary.activeUsers = ko.observable();
        this.summary.inactiveUsers = ko.observable();
        this.summary.firstUserConnectedAt = ko.observable();
        this.summary.lastUserConnectedAt = ko.observable();
        this.summary.lastUserActivityAt = ko.observable();
        this.summary.longestActivity = ko.observable();
        this.summary.shortestActivity = ko.observable();
        this.summary.medianActivity = ko.observable();
        this.summary.averageActivity = ko.observable();
        this.logger = function() {
            this.log = function(message) {
                console.log(message);
            };
            return this;
        }();

        this.init = function (realtimeSrvr) {
            if (that.disconnetHandler) {
                that.disconnetHandler();
            }
            if (!realtimeSrvr || !that[realtimeSrvr + 'InitHandler']) {
                return;
            }
            that.serverInfo(realtimeSrvr);
            that[realtimeSrvr + 'InitHandler'](that);
        };

        this.signalRInitHandler = signalRInit;
        this.nodejsInitHandler = nodejsInit;
        this.iisnodeInitHandler = iisnodeInit;
        this.disconnetHandler = null;

        function signalRInit() {
            var url = 'http://localhost/RealTimeSignalRServer/';
            var connection = $.hubConnection(url);
            connection.logging = true;
            var proxy = connection.createHubProxy('dashboardhub');
            proxy.on('message', function (data) {
                processReceivedData(data);

            });
            connection.start()
                .done(function() {
                    that.logger.log('Now connected to ' + url + ', connection ID=' + connection.id + ' UniqueId=');
                })
                .fail(function() { that.logger.log('Could not connect to ' + url); });

            that.disconnetHandler = function() {
                that.logger.log('Disconnected from ' + url);
            };
        }

        function nodejsInit() {
            var url = 'http://localhost:8888/dashboardhub';
            nodejsCommon(url);
        }

        function iisnodeInit() {
            var url = 'http://localhost:9999/dashboardhub';
            nodejsCommon(url);
        }

        function nodejsCommon(url) {
            var client = io.connect(url);

            client.on('message', function(data) {
                processReceivedData(data);
            });

            client.on('connect', function() {
                that.logger.log('Now connected to ' + url + ', connection ID=' + client.socket.sessionid + ' UniqueId=');
            });

            client.on('connect_failed', function() {
                that.logger.log('Could not connect to ' + url);
            });

            that.disconnetHandler = function() {
                that.logger.log('Disconnected from ' + url);
                client.disconnect();
            };
        }

        function processReceivedData(data) {
            if (data.type === 'data') {
                that.dataItems.unshift(data.message);
                that.summary.lastUserActivityAt(data.message.timestamp);
                that.logger.log('got data ');
            }

            if (data.type === 'greeting') {
                that.logger.log('unique id: ' + data.message);
            }
            
            if (data.type === 'userAdded') {
                that.logger.log('user added: ' + JSON.stringify(data.message));
                var user = data.message;
                augmentUser(user);
                that.users.unshift(user);
            }

            if (data.type === 'userRemoved') {
                that.logger.log('user removed: ' + JSON.stringify(data.message));
                var disconnectedUser = data.message;
                var match = _.find(that.users(), function(usr) {
                    return usr.connectionId === disconnectedUser.connectionId;
                });
                if (match) {
                    match.disconnectedAt(disconnectedUser.disconnectedAt);
                }
            }

            if (data.type === 'users') {
                that.logger.log('users: ' + JSON.stringify(data.message));
                $.each(data.message, function (index, item) {
                    var nextUser = item;
                    augmentUser(nextUser);
                    that.users.push(nextUser);
                });
            }
            
            if (data.type === 'summary') {
                that.logger.log('summary: ' + JSON.stringify(data.message));
                var summary = data.message;
                that.summary.activeUsers(summary.activeUsers);
                that.summary.inactiveUsers(summary.inactiveUsers);
                that.summary.firstUserConnectedAt(summary.firstUserConnectedAt);
                that.summary.lastUserConnectedAt(summary.lastUserConnectedAt);
                that.summary.lastUserActivityAt(summary.lastUserActivityAt);
                that.summary.longestActivity(summary.longestActivity);
                that.summary.shortestActivity(summary.shortestActivity);
                that.summary.medianActivity(summary.medianActivity);
                that.summary.averageActivity(summary.averageActivity);
            }
            
            function augmentUser(hubUser) {
                hubUser.disconnectedAt = ko.observable(hubUser.disconnectedAt);
                hubUser.userStatusCss = ko.computed(function () {
                    return hubUser.disconnectedAt() === null ? "userStatus statusActive" : "userStatus statusInactive";
                });
            }
        }

        return this;

    })();

    return dashboardViewModel;
});