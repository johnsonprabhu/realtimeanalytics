﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Hubs;
using Microsoft.AspNet.SignalR.Client.Transports;

namespace SignalRLoadSimulator
{
    internal class Program
    {
        private static string url = "http://localhost/RealTimeSignalRServer/";

        private static int createTimeDelay = 500;

        private static Random randomInterval = new Random();

        private static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                var delay = (i + 1) * createTimeDelay;
                var task = WaitForNewClient(delay, randomInterval.Next(10000, 30000));
               task.Wait();
            }

            Thread.Sleep(1000 * 1000);
        }

        private static async Task WaitForNewClient(int delay, int sendInterval)
        {
            await CreateClient(delay, sendInterval);
        }
    
        private static async Task<HubConnection> CreateClient(int delay, int sendInterval)
        {
            var clicks = 1;
            var disconnectTime = DateTime.Now.AddMilliseconds(sendInterval * randomInterval.Next(1, 100));

            var sendTimer = new System.Timers.Timer();
            sendTimer.Interval = sendInterval;
            sendTimer.Enabled = true;

            var hubConnection = new HubConnection(url);
            //hubConnection.TraceLevel = TraceLevels.All;
            //hubConnection.TraceWriter = Console.Out;
            IHubProxy proxy = hubConnection.CreateHubProxy("analyticshub");

            proxy.On("Message", message => ProcessReceivedData(message));

            hubConnection.Closed += () => Console.WriteLine("**************************** Connection closed.");
            hubConnection.Error += (e) => Console.WriteLine("Connection error: " + e.Message);

            await Task.Delay(delay);
            
            hubConnection.Start(new WebSocketTransport()).Wait();

            Console.WriteLine("Now connected to " + url + ", connection ID=" + hubConnection.ConnectionId + " UniqueId=");
            Console.WriteLine("send interval: " + sendInterval + ", disconnect after: " + disconnectTime);
            Console.WriteLine("current time: " + DateTime.Now);

            sendTimer.Elapsed += (sender, args) => proxy.Invoke("Send", BuildMessage(clicks++)).ContinueWith(
                task =>
                    {
                        Console.WriteLine(
                            "Invocation of send from " + hubConnection.ConnectionId + " to " + url + " succeeded at "
                            + DateTime.UtcNow);
                        if (DateTime.Now > disconnectTime)
                        {
                            sendTimer.Stop();
                            hubConnection.Stop();
                        }
                    });

            return hubConnection;
        }

        private static string BuildMessage(int clicks)
        {
            return string.Format("Time: {0}, Clicks: {1}", DateTime.UtcNow, clicks);
        }


        static void ProcessReceivedData(dynamic message)
        {
            if (message.type == "greeting")
            {
                Console.WriteLine("unique id: {0}", message.message);
            }
        }
    }
}
